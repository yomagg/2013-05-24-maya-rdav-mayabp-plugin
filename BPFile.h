/*
 * BPFile.h
 *
 *  Created on: Mar 19, 2013
 *      Author: magg
 */

#ifndef BPFILE_H_
#define BPFILE_H_

#include <string>
#include <vector>
#include <map>
#include <visitstream.h>
#include <stdlib.h>
#include "boost/multi_array.hpp"
#include "boost/unordered_map.hpp"
#include <set>
//#include <unordered_map>
#include "BPFile.h"
#include <InvalidVariableException.h>
#include <H5public.h> // hsize_t
#include "misc.h"

//NOTE: #include <mpi.h> *MUST* become before the adios includes.
#ifdef PARALLEL
#include <mpi.h>
#else
#define _NOMPI
#endif

// this is because default adios_read.h includes both adios_read_v2.h
// however the ADIOS_GROUP is defined in _v1.h and is not defined in _v2.h
// to enable that in this code you need to include either
// #  include "adios_read_v1.h" or set the -DADIOS_USE_READ_API_1 compiler flag to set
// the READ_API_1 or just define ADIOS_USE_READ_API_1
#ifdef ADIOS_USE_READ_API_1
#undef ADIOS_USE_READ_API_1
#endif
extern "C"
{
#include <adios_read.h>
}

#include "debug.h"

//#include "avtMayaBPFileFormat.h"
class dataset_entry;

namespace adios {

class Buffer;

//! to indicate unknown name of the variable read from ADIOS
const std::string ADIOS_NAME_UNKNOWN="/unknown_attr/unknown_thorn::unknown_var";


//! if we have a variable in the checkpoint called
//! I call this variable ADIOS variable /carpet_mglevel/KRANC2BSSNCHI::chi
//! carpet_mglevel is the MAYA_ATTR_NAME
//! KRANC2BSSNCHI is the MAYA_THORN_NAME
//! chi is the MAYA_VAR_NAME
//! the name of the attribute element for a MAYA VAR
const std::string ADIOS_VAR_MAYA_ATTR_NAME = "attr";
//! the name of the thorn element for a MAYA VAR
const std::string ADIOS_VAR_MAYA_THORN_NAME = "thorn";
//! the actual name of the MAYA variable
const std::string ADIOS_VAR_MAYA_VAR_NAME="var";


//! the attributes name
const std::string ADIOS_VAR_DATA_ATTR("data");
const std::string ADIOS_VAR_LEVEL_ATTR("level");
const std::string ADIOS_VAR_TIMESTEP_ATTR("timestep");
const std::string ADIOS_VAR_GRP_TIMELEVEL_ATTR("group_timelevel");
const std::string ADIOS_VAR_TIME_ATTR("time");
const std::string ADIOS_VAR_CCTK_BBOX_ATTR("cctk_bbox");
const std::string ADIOS_VAR_CARPET_MGLEVEL_ATTR("carpet_mglevel");
const std::string ADIOS_VAR_CCTK_NGHOSTZONES_ATTR("cctk_nghostzones");
const std::string ADIOS_VAR_ORIGIN_ATTR("origin");
const std::string ADIOS_VAR_DELTA_ATTR("delta");


// used in tests
//! this is the location of the bp file used for test methods
//! not a const variable because of the linking error (multiple definitions error)
//! don't have time to fight with this
#define TEST_BP_FILE "/rock/synchrobox/proj/w-ecl/2013-01-03-rdav-maya-adios-visit-plugin/MayaBP/tests/my.bp"

//! the name of the Maya AMR grid
#define MAYA_AMR_GRID "Maya AMR-grid"

//! this is the name of the variable in a checkpoint holding the
//! number of patches
#define PATCH_COUNT_VAR_NAME "P"

//! the id of the variable I am trying to visualize on the grid
#define KRANC2BSSNCHI_CHI_VAR_ID 2956
#define KRANC2BSSNCHI_CHI_LEVEL_VAR_ID 2957

//! the ADIOS root name for the variable I am trying to visualize
#define KRANC2BSSNCHI_CHI_VARNAME "KRANC2BSSNCHI::chi"
//! the name as it appears in Visit
#define KRANC2BSSNCHI_CHI_VARNAME_VIS "KRANC2BSSNCHI--chi"

//! used to open the checkpoint file
//#define BP_CHECKPOINT "/rock/maya-inputs/checkpoint.chkpt.tmp.it_768.bp"
// TODO this seems to be not needed anymore
//#define BP_CHECKPOINT "/rock/maya/debug/R1-adios-heffalin/checkpoint.chkpt.tmp.it_0.file_0.bp"

//! test use case amr mesh name
#define TEST_USE_CASE_AMR_GRID_NAME "Test AMR-grid"
//! test varname
#define TEST_USE_CASE_VAR_NAME "Var-Magic"

//! the location of the file for dbg purposes to dump hdf5 dataset_entries
#define DATASET_ENTRY_HDF5_FILE "/rock/maya-inputs/dbg_wrt_hdf5.txt"
//! the location of the file for dbg purposes to dump bp dataset_entries
#define DATASET_ENTRY_BP_FILE "/rock/maya-inputs/dbg_wrt_bp.txt"
class MayaVar;


	// This is what we get from the Maya checkpoint file
	// unsigned integer    /P                 scalar
	// unsigned integer    /patch_id          scalar
	// unsigned integer    /shape_dim_x       scalar
	// unsigned integer    /shape_dim_y       scalar
	// unsigned integer    /shape_dim_z       scalar
	// double              /data/YLM_DECOMP::temp3d         {45, 83, 113, 27}
	//  integer            /level/YLM_DECOMP::temp3d        {27}
	//  integer            /carpet_mglevel/YLM_DECOMP::temp3d  {27}
	//  integer            /timestep/YLM_DECOMP::temp3d        {27}
	//  integer            /group_timelevel/YLM_DECOMP::temp3d {27}
	//  double             /time/YLM_DECOMP::temp3d            {27}
	//  integer            /cctk_bbox/YLM_DECOMP::temp3d       {6, 27}
	//  integer            /cctk_nghostzones/YLM_DECOMP::temp3d{3, 27}
	//  double             /origin/YLM_DECOMP::temp3d          {3, 27}
	//  double             /delta/YLM_DECOMP::temp3d           {3, 27}
	//  integer            /iorigin/YLM_DECOMP::temp3d         {3, 27}
	//  unsigned long long /shape/YLM_DECOMP::temp3d           {3, 27}

	// /cctk_nghostzones/ - the order of the equations, usually is 3 but it can be more
	// /origin - coordinates, bottom corner of the grid
	// /delta - delta stride - different strides, factor of 2
    // /shape - The dimension of the box on for reading the data

class BPFile {
public:
	BPFile(const char *fname="");
	virtual ~BPFile();

	diag_t read_adios_var_to_dataset_entries(const char* varname_root, std::vector<dataset_entry> *p_vec);
	diag_t read_maya_attr_by_patch_id(const char *varname, Buffer *buf, uint64_t patch_id);
	diag_t read_maya_attr_by_patch_id(const char* varname,
			Buffer* buf,
			const uint64_t * count,
			const uint64_t *start,
			const int start_count);
	diag_t read_adios_var_dims(const char *varname, Buffer *buf);
	diag_t read_adios_maya_var_names(std::set<std::string> *p_names_set);

	diag_t open();
	diag_t close();
	bool is_open() const { return fp != NULL; }
	bool is_reflevel_read() const { return (ref_levels.size() > 0); }
	int num_timesteps();

	std::string get_file_name() const { return file_name; }
	int get_patch_count();
	int get_max_ndims();
	int get_max_reflevel();

	diag_t read_reflevel();


	static diag_t write_dataset_entry_to_txt(const char *filename,
								  const char* name_,
		                          const char* varname_,
		                          const int cycle_,
		                          const double time_,
		                          const int rl_,
		                          const int map_,
		                          const int factor_,
		                          const int type_,
		                          const int comps_,
		                          const double* origin_,
		                          const double* delta_,
		                          const int* iorigin_,
		                          const int ndims_,
		                          const hsize_t* dims_,
		                          const bool is_Cartesian_,
		                          const int* ghosts_,
		                          const int patch_id_);
	static diag_t read_dataset_entry_from_txt(const char *file_name,
			std::vector<dataset_entry>* p_vec);

	static bool supported_vars(ADIOS_VARINFO *avi);

	static bool run_tests();

	// testing functions
	static bool test_open();


	//! this holds the maya variables as read from the BP file
	//! the key is the adios_root_name i.e.KRANC2BSSNCHI_chi (the thorn_varname)
	boost::unordered_map<std::string, MayaVar> maya_vars;

	//! the refinement level layout - I assume it is the same for all
	//! variables; ref_levels[i] says that the i-th patch (starting from 0)
	//! stores the data at the ref_level[i] refinement level
	std::vector<int> ref_levels;


protected:
	//! the name of the file
	std::string file_name;

	//! the file handler to the ADIOS BP file
	ADIOS_FILE *fp;


private:
	diag_t read_adios_vars();
	diag_t clear_maya_vars();

	size_t get_adios_datatype_size(ADIOS_VARINFO *avi);
};

/**
 * This will store information about the particular Maya variable
 * as constructed from the ADIOS file.
 *
 * TODO this is weird but for some reason when I define the methods
 * outside this class when the visit tries to load the plugin it reports
 * and error that a symbol _tzgdMayaVar was not defined. So the current
 * workaround is just to inline the functions. Right now I don't have
 * time to fight with this.
 *
 */

class MayaVar {

private:
	/**
	 * initialize the MayaVar.
	 *
	 * @param name The name of the variable stored by ADIOS. It is expected
	 *             to be a valid Maya variable name as defined by @see
	 *             MayaVar::is_valid_maya_name()
	 * @return DIAG_OK Everything went great!
	 */
	diag_t init(const std::string &name){
		adios_root_name = get_adios_root_name(name);
		vtk_scalar = false;

		return DIAG_OK;
	}

public:
	/**
	 * constructs the MayaVar
	 * @param name The name of the variable stored by ADIOS. It is expected
	 *             to be a valid Maya variable name as defined by @see
	 *             MayaVar::is_valid_maya_name()
	 *             If the name is not provided then ADIOS_NAME_UNKNOWN is used.
	 */
	MayaVar(const std::string &name=ADIOS_NAME_UNKNOWN){
		init(name);
	}

	virtual ~MayaVar(){
		// all data seem to be allocated automatically so the proper
		// destructors should be called automatically as well
	}


	/**
	 * If the variable represents a VTK scalar
	 * @return true if this is a scalar
	 */
	bool is_vtk_scalar(){
		return vtk_scalar;
	}

	/**
	 * checks if the ADIOS_VARINFO is a scalar
	 * @param avi The variable to be checked
	 * @return true The variable is a scalar
	 *         false The variable is not a scalar meaning it is a variable
	 */
	static bool is_adios_varinfo_scalar(ADIOS_VARINFO *avi){
		return (0 == avi->ndim);
	}


	/**
	 * defines if the adios varinfo should be considered as
	 * a VTK scalar
	 *
	 * Right now always true
	 * @param avi
	 * @return true if yes
	 *         false if not, and this indicates it is a vector
	 * TODO ALWAYS RETURNS TRUE !!!!!!!!!!!!!!!!!
	 */
	static bool is_adios_varinfo_vtk_scalar(ADIOS_VARINFO *avi){
		p_todo("Now everything I treat as a VTK scalar!\n");
		return true;
	}


	/**
	 * The valid "ADIOS Maya variable" name is
	 * /data/KRANC2BSSNCHI::chi
	 *
	 * /attribute/thorn_name::variable_name
	 *
	 * This method checks if the string is a valid ADIOS Maya
	 * variable name by detecting "::"
	 *
	 * @param adios_var_name the string to be checked
	 * @return true The string represents the valid ADIOS Maya var name
	 *         false The string does not represent the valid Maya var name
	 */
	static bool is_valid_maya_name(const std::string &adios_var_name){
		// I detect by detecting the "::"
		return (adios_var_name.find("::") != std::string::npos);
	}

	/**
	 * checks if the it is the maya var is prefixed with /data
	 * @param adios_var_name - should be a valid maya var
	 * @return true if it is /data/thorn::name
	 *         false if it is not /data/thorn::name
	 */
	static bool is_data_prefix(const std::string &adios_var_name){
		return (adios_var_name.find("/data/") != std::string::npos);
	}

	/**
	 * checks if it is the adios name is prefixed with /level
	 * @param adios_var_name - should be a valid maya var name
	 * @return true if it is /level/thorn::name
	 *         false if it is not /level/thorn::name
	 */
	static bool is_level_prefix(const std::string &adios_var_name){
		return (adios_var_name.find("/level/") != std::string::npos);
	}

	/**
	 * returns the root name of the ADIOS var name, e.g. for ADIOS var name /data/KRANC2BSSNCHI::chi
	 * it should return KRANC2BSSNCHI::chi
	 * It doesn't check if this is the in arg is a NULL string or something
	 *
	 * @param adios_var_name The name read from the ADIOS BP file
	 * @return The "Maya variable" name defined by the thorn and the name of the variable
	 */

	static std::string get_adios_root_name(const std::string &adios_var_name){
		size_t first = adios_var_name.find('/');
		size_t second = -1;
		std::string ret;

		if (std::string::npos != first){
			second = adios_var_name.find('/', first+1);
			if (std::string::npos != second)
				ret = adios_var_name.substr(second);
		}

		return ret;
	}
	/**
	 * returns the "Maya variable" name, e.g. for ADIOS var name /data/KRANC2BSSNCHI::chi
	 * it should return KRANC2BSSNCHI_chi
	 * It doesn't check if this is the in arg is a NULL string or something
	 *
	 * @param adios_var_name The name read from the ADIOS BP file
	 * @return The "Maya variable" name defined by the thorn and the name of the variable
	 */
	static std::string get_maya_var_name(const std::string &adios_var_name){
		std::map<std::string, std::string> parsed_adios_var_name = parse_maya_var(adios_var_name);

		std::stringstream sstm;
		sstm << parsed_adios_var_name[ADIOS_VAR_MAYA_THORN_NAME] << "::" << parsed_adios_var_name[ADIOS_VAR_MAYA_VAR_NAME];

		return sstm.str();
	}
	/**
	 * returns the name of the Maya variable. So if the name is:
	 * /time/SPHERICALSURFACE::sf_delta_theta_estimate[1] it should return
	 * a map containing ADIOS_VAR_MAYA_ATTR_NAME: time
	 *                  ADIOS_VAR_MAYA_THORN_NAME: SPHERICALSURFACE
	 *                  ADIOS_VAR_MAYA_VAR_NAME:  sf_delta_theta_estimate[1]
	 *
	 * @param adios_var_name The name of the variable stored in the BP checkpoint file
	 * @return a map containing an attribute, thorn, and the variable name separated
	 *
	 */
	static std::map<std::string, std::string> parse_maya_var(const std::string &adios_var_name){
		std::map<std::string, std::string> parsed_str;

		// get the attribute name
		size_t first = adios_var_name.find("/");
		size_t second = -1;
		size_t third = -1;
		if (first != std::string::npos) {
			second = adios_var_name.find("/", first + 1);
			if (second != std::string::npos) {
				third = adios_var_name.find("::", second + 1);
			}
		}

		// if adios_var_name has the right string then
		// first should hold the start of the attribute,
		// second should hold the start of thorn name
		// third should hold the start of the actual variable name
		// (of course +1/-1 depending on the index mathematics)

		if (first != std::string::npos && second != std::string::npos) {
			parsed_str[ADIOS_VAR_MAYA_ATTR_NAME] = adios_var_name.substr(
					first + 1, second - first - 1);
		}

		if (second != std::string::npos && third != std::string::npos) {
			parsed_str[ADIOS_VAR_MAYA_THORN_NAME] = adios_var_name.substr(
					second + 1, third - second - 1);
		}

		if (third != std::string::npos) {
			// third+2 because of two characters "::"
			parsed_str[ADIOS_VAR_MAYA_VAR_NAME] = adios_var_name.substr(
					third + 2);
		}

		return parsed_str;
	}

	// for testing purposes

	/**
	 * for testing purposes
	 * @return false means one of the tests failed
	 *         true  means all the tests passed
	 */
	static bool test_parse_maya_var(){
		bool diag = true;

		// 1. check the typical case
		std::map<std::string, std::string> maya_var = parse_maya_var(
				"/time/SPHERICALSURFACE::sf_delta_theta_estimate[1]");

		if (maya_var[ADIOS_VAR_MAYA_ATTR_NAME].compare("time") != 0) {
			p_test_failed("attribute wrong\n");
			return false;
		}
		if (maya_var[ADIOS_VAR_MAYA_THORN_NAME].compare("SPHERICALSURFACE")
				!= 0) {
			p_test_failed("thorn wrong\n");
			return false;
		}
		if (maya_var[ADIOS_VAR_MAYA_VAR_NAME].compare(
				"sf_delta_theta_estimate[1]") != 0) {
			p_test_failed("var wrong\n");
			return false;
		}

		// 2. TODO test the missing variables cases

		p_test_passed("PASSED\n");
		return true;
	}
	static bool test_get_maya_var_name(){
		std::string adios_var_name = "/data/KRANC2BSSNCHI::chi";

		// 1. check the typical situation
		std::string maya_var_name = get_maya_var_name(adios_var_name);
		if (maya_var_name.compare("KRANC2BSSNCHI::chi") != 0){
			p_test_failed("getting the correct maya var name\n");
			return false;
		}

		p_test_passed("PASSED\n");
		return true;
	}

	static bool test_is_valid_maya_name(){
		// 1. try to detect the valid adios maya var name
		std::string valid_adios_maya_var_name = "/data/KRANC2BSSNCHI::chi";

		if (!is_valid_maya_name(valid_adios_maya_var_name)){
			p_test_failed("\n");
			return false;
		}

		// 2. try with the invalid adios maya var name
		std::string invalid = "npatches";
		if (is_valid_maya_name(invalid)){
			p_test_failed("\n");
			return false;
		}

		p_test_passed("PASSED\n");
		return true;
	}

	static bool test_get_adios_root_name(){
		// 1. try to detect the valid adios maya var name
		std::string var = "/data/KRANC2BSSNCHI::chi";
		std::string res = get_adios_root_name(var);

		p_error("res=%s\n", res.c_str());
		if (res.compare("/KRANC2BSSNCHI::chi") != 0){

			p_test_failed("\n");
			return false;
		}

		p_test_passed("PASSED\n");
		return true;
	}
	/**
	 *
	 * @return true if all tests went successfully
	 *         false at least one test failed.
	 */
	static bool run_tests(){
		if (!test_parse_maya_var()){
			return false;
		}
		if (!test_get_maya_var_name()){
			return false;
		}
		if (!test_is_valid_maya_name()){
			return false;
		}
		if (!test_get_adios_root_name()){
			return false;
		}

		p_test_passed("ALL TESTS: PASSED!\n");

		return true;
	}



	// normally ADIOS variable with /data prefix will have at least 1D data
	// and this should be considered as a scalar for the vtk
	bool vtk_scalar;			// if the variable should be treated as a scalar for VTK

	//! /YLM_DECOMP::temp3d, the name of the thorn and the name of the variable
	//! as ADIOS records it and as defined by @see MayaVar::get_adios_root_name()
	std::string adios_root_name;
};

/**
 * This class is a wrapper for the ADIOS scalars
 */
class Scalar{
public:

	/**
	 * reads the adios scalar variable but only checks the value field;
	 * @param fp a pointer to the adios file
	 * @param p a pointer to the value you want to read as a scalar value;
	 *          the pointer needs to point to an allocated memory location
	 * @param var_name The name of the variable to be read and as a scalar value
	 * @return DIAG_OK if everything ok
	 *         != DIAG_OK not a scalar detected
	 *         InvalidVariableException if some adios related errors
	 * TODO this method works but in general it should be changed
	 * since and ADIOS scalar can be written by different processes and
	 * each process can write a different value depending on the ADIOS block
	 * refer to @see read_scalar(ADIOS_FILE *fp, int varid) in tests/general_rw/chkpt_reader.c
	 * It works for the P variable as all processes writes the same value
	 * so you can use that function in particular case but it is not general
	 * enough
	 */
	template<class T> static diag_t read_checkpoint_P_scalar_value(ADIOS_FILE *fp, T *p, const char* var_name) {
		ADIOS_VARINFO *avi = NULL;
		diag_t diag = DIAG_OK;
		errmsg_t errmsg;

		// inquire the variable
		avi = adios_inq_var(fp, var_name);

		if (!avi) {
			throw_exception1(InvalidVariableException, "(%d): %s\n", adios_errno, adios_errmsg());
		}

		if (avi->ndim) {
			p_error("Detected not a scalar. Returning ...\n");
			adios_free_varinfo(avi);
			return DIAG_ERR_NOT_A_SCALAR;
		}

		if ( (diag = convert<T>(p, avi)) != DIAG_OK){
			p_error("Issues with conversion. Returning ...\n");
			adios_free_varinfo(avi);
			return DIAG_ERR;
		}

		adios_free_varinfo(avi);

		return DIAG_OK;
	}

	/**
	 *
	 * @param p the memory where we want to hold the converted variable
	 * @param avi The variable we are converting
	 * @return != DIAG_OK adios_unknown type detected
	 *         DIAG_OK everything went fine
	 */
	template<class T> static diag_t convert(T *p, ADIOS_VARINFO *avi) {
		if (NULL == p || NULL == avi){
			p_error("The NULL pointer detected ... \n");
			return DIAG_ERR_NULL_PTR;
		}
		size_t sz = 0;
		if ( adios_unknown != avi->type )
			sz = adios_type_size(avi->type, avi->value);
		else {
			p_error("adios_unknown type detected ... ATS\n");
			return DIAG_ERR_NOT_SUPPORTED;
		}

		if (sz > 0){
			// to distinguish cases when this is a scalar that holds 0 value
			// if avi->value is NULL then I interpret this as a zero-value scalar
			if (NULL != avi->value)
				memcpy(p, avi->value, sz);  // interpret as a non-zero value scalar
			else
				memset(p, 0, sz);           // interpret as a zero-value scalar
		}

		return DIAG_OK;
	}
};

/**
 * The helper type for class Buffer to know what type is stored
 * in the Buffer.buf
 */
enum el_type {
	EL_UNKNOWN,     //!< EL_UNKNOWN
	EL_FLOAT,       //!< EL_FLOAT
	EL_DOUBLE,      //!< EL_DOUBLE
	EL_INT,         //!< EL_INT
	EL_UNSIGNED_INT,//!< EL_UNSIGNED_INT
	EL_UNSIGNED_LONG//!< EL_UNSIGNED_LONG
};
/**
 * the helper class for storing information about the buffer
 */
class Buffer {
public:
	Buffer() : buf(NULL), buf_el_count(0), buf_el_size(0), buf_el_type(EL_UNKNOWN) {}
	~Buffer() {}

	/**
	 * frees the resources allocated to the buffer and
	 * zeros the buf element counts and size
	 */
	void release(){
		free(buf);
		buf = NULL;
		buf_el_count = 0;
		buf_el_size = 0;
		buf_el_type = EL_UNKNOWN;
	}

	bool is_float(){
		return (EL_FLOAT == buf_el_type);
	}

	bool is_double(){
		return (EL_DOUBLE == buf_el_type);
	}

	bool is_int(){
		return (EL_INT == buf_el_type);
	}

	bool is_unsigned_int(){
		return (EL_UNSIGNED_INT == buf_el_type);
	}

	bool is_unsigned_long(){
		return (EL_UNSIGNED_LONG == buf_el_type);
	}

	bool is_unknown(){
		return (EL_UNKNOWN == buf_el_type);
	}

	/**
	 * converts the adios datatype to the el_type
	 * @param avi (in) The adios variable that we want to get the type
	 * @param my_type (out) The output, converted type
	 * @return DIAG_OK everything went fine
	 *         != DIAG_OK errors
	 */
	diag_t convert_adios_datatype_to_el_type(const ADIOS_VARINFO *avi, enum el_type * my_type){
		diag_t diag = DIAG_OK;
		enum el_type ret_type;
		if( !avi ){
			p_error("avi is a NULL pointer. Quitting ...\n");
			return DIAG_ERR_NULL_PTR;
		}

		switch( avi->type ){
		case adios_integer: *my_type = EL_INT; break;
		case adios_unsigned_integer: *my_type = EL_UNSIGNED_INT; break;
		case adios_real: *my_type = EL_FLOAT; break;
		case adios_double: *my_type = EL_DOUBLE; break;
		case adios_unsigned_long: *my_type = EL_UNSIGNED_LONG; break;
		default:
			*my_type = EL_UNKNOWN;
			break;
		}

		return DIAG_OK;
	}

	/**
	 * prints what was read by adios_perform_reads
	 *
	 * @param buf The buffer to be printed
	 * @return DIAG_OK
	 */
	static diag_t cout_buffer(Buffer *buf){

		// TODO until I find something smarter
		switch( buf->buf_el_type ){
		case EL_INT:
			{
				cout << "el_type=(int) " << buf->buf_el_type << ": ";

				int *p_buf = (int*)buf->buf;
				// print all steps
				for(int i = 0; i < buf->buf_el_count; i ++){
					cout << p_buf[i] << " ";
				}
			}
			break;
		case EL_UNSIGNED_LONG:
		{
			cout << "el_type=(unsigned long) " << buf->buf_el_type << ": ";

			unsigned long *p_buf = (unsigned long*)buf->buf;
			// print all steps
			for(int i = 0; i < buf->buf_el_count; i ++){
				cout << p_buf[i] << " ";
			}
		}
			break;
		case EL_DOUBLE:
		{
			cout << "el_type=(double) " << buf->buf_el_type << ": ";
			double *p_buf = (double*)buf->buf;
			// print all steps
			for(int i = 0; i < buf->buf_el_count; i ++){
				cout << p_buf[i] << " ";
			}
		}
		break;
		case EL_FLOAT:
		{
			cout << "el_type=(float) " << buf->buf_el_type << ": ";

			float *p_buf = (float*)buf->buf;
			// print all steps
			// print all steps
			for(int i = 0; i < buf->buf_el_count; i ++){
				cout << p_buf[i] << " ";
			}
		}
		break;

		default:
			cout << "Printing of the data not supported";
			break;
		}
		cout << endl;

		return DIAG_OK;
	}


	//! the pointer to the buffer
	void *buf;
	//! how many elements I have in the buffer
	uint64_t buf_el_count;
	//! the size of the element
	uint64_t buf_el_size;
	//! the type of the element stored in the buffer
	enum el_type buf_el_type;
};
} // Namespace ADIOS
#endif /* BPFILE_H_ */
