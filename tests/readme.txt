# readme.txt
# Created on: Mar 26, 2013
# Author: Magda S. aka Magic Magg magg dot gatech at gmail.com
#

to enable testing

* my.bp - the adios bp file; maybe used for testing purposes
          the file has been generated with bp-write/write-test

* chkpt_reader.c - the test file for reading of the checkpoint dumped by
                   Jeremy's ADIOS writer
* general_rw - how to read ADIOS multidimensial arrays; this
               demonstrates how to read and write scalaras, 
               1D and 2D and 3D variables with adios; the used API is outdated
# EOF

