/**
 * bp_utils.c
 *
 *  Created on: Apr 19, 2013
 *  Author: Magda Slawinska aka Magic Magg magg dot gatech at gmail.com
 */

//NOTE: #include <mpi.h> *MUST* become before the adios includes.
#ifdef PARALLEL
#include <mpi.h>
#else
#define _NOMPI
#endif

// this is because default adios_read.h includes both adios_read_v2.h
// however the ADIOS_GROUP is defined in _v1.h and is not defined in _v2.h
// to enable that in this code you need to include either
// #  include "adios_read_v1.h" or set the -DADIOS_USE_READ_API_1 compiler flag to set
// the READ_API_1 or just define ADIOS_USE_READ_API_1
#ifdef ADIOS_USE_READ_API_1
#undef ADIOS_USE_READ_API_1
#endif
#include <adios_read.h>
// #include <adios_types.h> // for adios_datatypes


/**
 * Print the avi variable
 *
 * @param fp
 * @param avi
 * @return 0
 */
int print_avi(ADIOS_FILE *fp, ADIOS_VARINFO *avi){
	int i = 0;

	printf("================================\n");
	printf("Var name:  %s\n", fp->var_namelist[avi->varid]);
	printf("Var index: %d\n", avi->varid);
	printf("Var type: %s\n", adios_type_to_string(avi->type));
	if( avi->ndim ){
		printf("This is var: ndim=%d\n", avi->ndim);
		for(i = 0; i < avi->ndim; i ++){
			printf("dim[%d]=%ld ", i, avi->dims[i]);
		}
		printf("\n");
	} else {
		printf("This is scalar\n");
	}
	if( 1 == avi->global)
		printf("Global view\n");
	else
		printf("pieces written by writers without defining a global view\n");

	printf("Var nsteps: %d (always at least one step)\n", avi->nsteps);
	printf("Var value: %p (NULL for array)\n", avi->value);

	printf("Sum of all blocks, all steps: %d\n", avi->sum_nblocks);
	printf("Nblocks per step: ");
	for( i = 0; i < avi->nsteps; i ++){
		printf("Step[%d]=%d ", i, avi->nblocks[i]);
	}

	printf("\n");
	printf("================================\n");

	return 0;
}

/**
 * Print the avi statistics variable
 *
 * @param fp
 * @param avi
 * @return 0
 */
int print_stats(ADIOS_FILE *fp, ADIOS_VARINFO *avi){
	int i = 0;

	if (adios_inq_var_stat(fp, avi, 0, 0) != 0){
		printf("(%d) %s\n", adios_errno, adios_errmsg());
		return -1;
	}

	if (avi->statistics && avi->statistics->min && avi->statistics->max){

		printf("Global stats for: (%d) %s\n", avi->varid, fp->var_namelist[avi->varid]);
		switch(avi->type){
		case adios_integer:
			printf("Min: %d\n", *(int*)avi->statistics->min);
		    printf("Max: %d\n", *(int*)avi->statistics->max);
		    break;
		case adios_double:
			printf("Min: %f\n", *(double*)avi->statistics->min);
			printf("Max: %f\n", *(double*)avi->statistics->min);
			break;
		default: printf("No stats for this avi->type\n"); break;
		}
	} else {
		printf("Stats switched off in the bp file\n");
	}
	return 0;
}

/**
 * get size of the avi_type
 * @param avi To get the avi->type
 * @return -1 if the variable is not supported
 *         the size of the avi->type in bytes
 */
int get_avi_type_size(ADIOS_VARINFO *avi){
	int avi_type_size = -1;

	if (adios_integer == avi->type) {
		avi_type_size = sizeof(int);
	} else if (adios_double == avi->type) {
		avi_type_size = sizeof(double);
	} else if (adios_real == avi->type) {
		avi_type_size = sizeof(float);
	} else {
		printf("Error: Sizing of this variable not supported. Quitting ...\n");
	}

	return avi_type_size;
}

/**
 * prints what was read by adios_perform_reads
 *
 * @param p The pointer to the buffer
 * @param avi The variable to get the info about the type of the variable
 * @param count The number of elements in the buffer of type avi->type
 * @return 0
 */
int p_perform_reads_buffer(void * p, ADIOS_VARINFO *avi, uint64_t count){

	int i = 0;

	if (adios_integer == avi->type ){
		int *p_int_buf = (int*)p;
		// print all steps
		for( i = 0; i < count; i ++){
			printf("%d ", p_int_buf[i]);
		}
		printf("\n");
	} else if (adios_double == avi->type){
		double *p_double_buf = (double*)p;
		// print all steps
		for( i = 0; i < count; i ++){
			printf("%f ", p_double_buf[i]);
		}
		printf("\n");
	} else if (adios_real == avi->type){
		float *p_float_buf = (float*)p;
		// print all steps
		for( i = 0; i < count; i ++){
			printf("%f ", p_float_buf[i]);
		}
		printf("\n");
	} else {
		printf("Printed type not supported\n");
	}

	return 0;
}
