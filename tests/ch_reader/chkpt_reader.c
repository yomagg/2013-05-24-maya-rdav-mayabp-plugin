/**
 * chkpt_reader.c
 *
 *  Created on: Apr 19, 2013
 *  Author: Magda Slawinska aka Magic Magg magg dot gatech at gmail.com
 *
 *  The main part of the file have been stolen from MayaParticleBP/tests
 */

//NOTE: #include <mpi.h> *MUST* become before the adios includes.
#ifdef PARALLEL
#include <mpi.h>
#else
#define _NOMPI
#endif

// this is because default adios_read.h includes both adios_read_v2.h
// however the ADIOS_GROUP is defined in _v1.h and is not defined in _v2.h
// to enable that in this code you need to include either
// #  include "adios_read_v1.h" or set the -DADIOS_USE_READ_API_1 compiler flag to set
// the READ_API_1 or just define ADIOS_USE_READ_API_1
#ifdef ADIOS_USE_READ_API_1
#undef ADIOS_USE_READ_API_1
#endif
#include <adios_read.h>
// #include <adios_types.h> // for adios_datatypes

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <assert.h>

#include "bp_utils.h"

//! the offset for reading bounding box and global
#define OFFSET 3
//! the bounding box size
#define BBOX 4

//! this is the varid of the /data/KRANC2BSSNCHI::chi
//! as in the  /rock/maya-inputs/checkpoint.chkpt.tmp.it_193.bp
//! I know how the variable should look like
#define KRANC2BSSNCHI_CHI_VAR_ID 2956

void help(char **argv){
	printf("Usage: %s file-to-be-read\n", argv[0]);
	printf("E.g.: %s /rock/maya-inputs/checkpoint.chkpt.tmp.it_193.bp\n", argv[0]);
}

/**
 * The second version of scalar with blocks
 * this is for a test file written by bp-write/write-test
 *
 * For clarity reasons I skip checking the output of the adios_xxx calls
 *
 * @param fp
 * @param varid
 * @return 0 everything fine
 *         -1 something wrong
 */
int read_scalar(ADIOS_FILE *fp, int varid){
	ADIOS_VARINFO *avi = NULL;

	// inquire the variable
	avi = adios_inq_var_byid(fp, varid);

	if (!avi) {
		printf("%s\n", adios_errmsg());
		return -1;
	}

	if (avi->ndim){
		// this is not a scalar
		printf("This routine doesn't support reading ADIOS variables. Quitting ...\n");
		adios_free_varinfo(avi);

		return 0;
	}

	// we need to read all blocks; get some additional information about
	// the blocks
	if (adios_inq_var_blockinfo(fp, avi) != 0){
		printf("%s\n", adios_errmsg());
		return -1;
	}

	print_avi(fp, avi);

	int i ;
	// this buffer will held the block we are want to write our selection out
	// the length is arbitrary to hold all possible scalars
	char buf[100];
	memset(buf, 0, sizeof(buf));

	for (i = 0; i < avi->sum_nblocks; i++){
		ADIOS_SELECTION *s = adios_selection_writeblock(i);

		if (adios_schedule_read(fp, s, fp->var_namelist[varid], 0, 1, &buf) != 0){
			printf("%s\n", adios_errmsg());
			return -1;
		}
		if (adios_perform_reads(fp, 1) != 0){
			printf("%s\n", adios_errmsg());
			return -1;
		}

		printf("Block[%d]=", i);
		switch(avi->type){
			case adios_integer: printf("%d\n", *(int*)buf); break;
			case adios_real:    printf("%f\n", *(float*)buf); break;
			case adios_double:  printf("%f\n", *(double*)buf); break;
			case adios_unsigned_integer: printf("%d\n", *(unsigned int*)buf); break;
			default: printf("Printing not supported of this avi->type\n"); break;
		}
	}

	adios_free_varinfo(avi);

	return 0;
}


/**
 * Reads the variable assuming that it was written with a global view
 * this is for the /rock/maya-inputs/checkpoint.chkpt.tmp.it_193.bp file
 * @param fp
 * @param varid
 * @return
 */
int read_variable_global(ADIOS_FILE *fp, int varid){
	ADIOS_VARINFO *avi = NULL;

	// inquire the variable
	avi = adios_inq_var_byid(fp, varid);

	if (!avi) {
			printf("%s\n", adios_errmsg());
			return -1;
	}

	if (0 == avi->ndim){
		// this is not a variable
		printf("This routine doesn't support reading ADIOS scalars. Quitting ...\n");
		adios_free_varinfo(avi);
		return 0;
	}

	print_avi(fp, avi);
	print_stats(fp, avi);

	void *buf = NULL;
	// the size of the element in the buf
	size_t buf_el_size = 0;
	// in checkpoint should be always one step
	size_t buf_el_count = avi->nsteps;
	int i = 0;
	// now multiply it by the number of dimension
	for(i = 0; i < avi->ndim; i++){
		buf_el_count *= avi->dims[i];
	}

	// get the size of the avi->type
	if ( (buf_el_size = get_avi_type_size(avi)) == -1 ){
		printf("Not supported variable error. Quitting ...\n");
		adios_free_varinfo(avi);
		return -1;
	}

	buf = malloc(buf_el_size * buf_el_count);

	if( !buf ){
		printf("Malloc error. Quitting ...\n");
		adios_free_varinfo(avi);
		return -1;
	}

	// read avi->nsteps
	if (adios_schedule_read(fp, NULL, fp->var_namelist[varid], 0, avi->nsteps, buf) != 0){
		printf("%s\n", adios_errmsg());
		adios_free_varinfo(avi);
		free(buf);
		return -1;
	}
	if (adios_perform_reads(fp, 1) != 0){
		printf("%s\n", adios_errmsg());
		adios_free_varinfo(avi);
		free(buf);
		return -1;
	}

	// uncomment this to print the buffer of read variables
	// a lot of numbers
	p_perform_reads_buffer(buf, avi, buf_el_count);

	if (avi->type == adios_double && varid == KRANC2BSSNCHI_CHI_VAR_ID ){
		double *p_double_buf = (double*)buf;
		// this is to print the bounding box [2,2,2,1] with the
		// offset [0,0,0,0]; @see read_variable_global_bounding_box
		// see copybook notes how I figured this out
		// those values should be identical with values
		// printed from read_variable_global_bounding_box
		printf("The below values should be identical with values got from bounding box\n");
		printf("See: read_variable_global_bounding_box()\n");
		printf("%f %f ", p_double_buf[0], p_double_buf[324]);
		printf("%f %f ", p_double_buf[324*22], p_double_buf[324*23]);
		printf("%f %f ", p_double_buf[324*22*26], p_double_buf[324*22*26+324]);
		printf("%f %f ", p_double_buf[324*22*27], p_double_buf[324*22*27+324]);
		printf("\n");
	}


	adios_free_varinfo(avi);
	avi = NULL;
	free(buf);
	buf = NULL;

	return 0;
}

/**
 * This reads a bounding box for a part.bp file
 * @param fp
 * @param varid
 * Read the bounding box
 * @return 0 everything went fine
 *         -1 some errors
 */
int read_variable_global_bounding_box(ADIOS_FILE *fp, int varid){
	ADIOS_VARINFO *avi = NULL;

	avi = adios_inq_var_byid(fp, varid);

	if (!avi) {
		printf("%s\n", adios_errmsg());
		return -1;
	}

	if (0 == avi->ndim){
		// this is not a variable
		printf("This routine doesn't support reading ADIOS scalars. Quitting ...\n");
		adios_free_varinfo(avi);
		return 0;
	}

	print_avi(fp, avi);

	void *buf = NULL;
	// the size of the element in the buf
	size_t buf_el_size = 0;
	// how many elements the buffer will have
	size_t buf_el_count = 0;
	int i = 0;

	// get the size of the avi->type
	if ( (buf_el_size = get_avi_type_size(avi)) == -1 ){
		printf("Not supported variable error. Quitting ...\n");
		adios_free_varinfo(avi);
		return -1;
	}

	// the parameters for the bounding box
	const int BBOX_X = 2;
	const int BBOX_Y = 2;
	const int BBOX_Z = 2;
	const int BBOX_PATCH = 1;
	uint64_t count[] = {BBOX_X, BBOX_Y, BBOX_Z, BBOX_PATCH};
	uint64_t step = 1;
	// from the beginning, from PATCH_NO
	const int PATCH_NO = 0;
	uint64_t offsets[] = { 0, 0, 0, PATCH_NO};

	if (4 == avi->ndim){
		// I read only one patch
		buf_el_count = count[0] * count[1] * count[2] * count[3];
		buf = malloc(buf_el_size * buf_el_count);
	} else {
		printf("Only 4D variables supported. Quitting ...\n");
		adios_free_varinfo(avi);
		return 0;
	}

	ADIOS_SELECTION *s = adios_selection_boundingbox( avi->ndim, offsets, count);
	if (!s){
		printf("(%d)%s\n", adios_errno, adios_errmsg());
		adios_free_varinfo(avi);
		free(buf);
		return -1;
	}

	// in the chkpt file there is only one timestep
	if (adios_schedule_read(fp, s, fp->var_namelist[varid], 0, 1, buf) != 0){
		printf("%s\n", adios_errmsg());
		adios_free_varinfo(avi);
		free(buf);
		return -1;
	}

	if (adios_perform_reads(fp, 1) != 0){
		printf("%s\n", adios_errmsg());
		adios_free_varinfo(avi);
		free(buf);
		return -1;
	}

	p_perform_reads_buffer(buf, avi, buf_el_count );

	adios_free_varinfo(avi);
	avi = NULL;
	adios_selection_delete(s);
	s = NULL;
	free(buf);
	buf = NULL;

	return 0;
}

int main(int argc, char**argv){

	if (1 == argc){
		help(argv);
		exit(0);
	}

	char bp_file_name[500];

	strcpy(bp_file_name, argv[1]);
	printf("Reading file: %s\n", bp_file_name);

	printf("ADIOS: initialization started ...\n");
	// this will hold parameters for adios read init method
	char * init_params = "verbose=4; show hidden_attrs";
	// used for diagnostic purposes
	int diag;
	// for storing error messages
	char errmsg[1024];

	//! handler to the adios file
	ADIOS_FILE *fp;

	// not parallel read (sequential read)
	if ((diag = adios_read_init_method(ADIOS_READ_METHOD_BP, 0, init_params)) != 0){
		printf("Problems with adios_read_init_method %s\n",  adios_errmsg());
		return diag;
	}

	printf("Initializing read ADIOS: SUCCESS!\n");

	// now open the file
	if ((fp = adios_read_open_file(bp_file_name, ADIOS_READ_METHOD_BP, 0)) == NULL ) {
		printf("Error opening bp file %s\n", adios_errmsg());
	}

	printf("ADIOS BP file: %s\n", bp_file_name);
	printf("# of variables: %d\n", fp->nvars);
	printf("# of attributes: %d\n", fp->nattrs);
	printf("Last step: %d starting from %d\n", fp->last_step, fp->current_step);

	// read variables
	int vr = 0;
	for (vr = 0; vr < fp->nvars; vr++) {

		if ( 0 == vr || 1 == vr){
			// uncomment/comment if you want to read scalars or not, respectively
			//read_scalar(fp, vr);
		}

		if (KRANC2BSSNCHI_CHI_VAR_ID == vr){
			// we focus on the /data/KRANC2BSSNCHI::chi variable
		/*	// comment/uncomment to read a variable via bounding_box
		    if (-1 == read_variable_global_bounding_box(fp, vr)){
				printf("Errors. Quitting ... \n");
				break;
			}

			if (-1 == read_variable_global(fp, vr)){
				printf("Errors. Quitting ... \n");
				break;
			} */
		}

		if (3 == vr){
			if (-1 == read_variable_global(fp, vr)){
				printf("Errors. Quitting ... \n");
				break;
			}
		}
		// I hope it will give me /level/KRANC2BSSNCHI::chi
		// all other values
		/*if ( vr > KRANC2BSSNCHI_CHI_VAR_ID && vr <= KRANC2BSSNCHI_CHI_VAR_ID + 9){
			printf("----------------------------------------------\n");
			printf("Reading the /xxx/KRANC2BSSNCHI::chi\n");
			printf("varid=%d should be: /xxxx/KRANC2BSSNCHI::chi\n", vr);
			printf("See if p_perform_reads_buffer(buf, avi, buf_el_count); in read_variable_global() is commented or not\n");
			if (-1 == read_variable_global(fp, vr)){
				printf("Errors. Quitting ... \n");
				break;
			}
		}*/
	}

	if( (diag = adios_read_close(fp)) != 0){
		printf("adios: (%d):%s\n", adios_errno, adios_errmsg());
	}
	fp = NULL;

	return 0;
}

