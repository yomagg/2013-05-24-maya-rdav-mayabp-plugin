/* 
 * ADIOS is freely available under the terms of the BSD license described
 * in the COPYING file in the top level directory of this source distribution.
 *
 * Copyright (c) 2008 - 2009.  UT-BATTELLE, LLC. All rights reserved.
 */

/*
 * ADIOS example from the User's manual
 *
 * Read back data written by 2_adios_posix.
 * Use the same method and the same number of processes that were used
 * for writing the data with 2_adios_posix.
 *
 */
#include <stdio.h>
#include "mpi.h"
#include "adios.h"
int main (int argc, char ** argv) 
{
    char        filename [256];
    int         rank;
    int         NX = 10;
    int			nx = 0;
    double      t[NX];
    char        result[1024], s[32];
    int         i,j;

    // sizes of the 2D variable
    // TODO if I set values to X=0, Y=0 then I got error,
    // those values are not read in adios_read, so they have to be correctly set
    // they are correctly flushed out after call adios_close()
    // I sent email ot Norbert about this behavior
    int X = 2,  Y = 3;

    /* ADIOS variables declarations for matching gread_temperature.ch */
    int         adios_err;
    uint64_t    adios_groupsize, adios_totalsize, adios_buf_size;
    int64_t     adios_handle;
    MPI_Comm    comm =  MPI_COMM_WORLD;

    MPI_Init (&argc, &argv);
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);
    sprintf (filename, "restart.bp");
    adios_init ("config.xml");
    adios_open (&adios_handle, "temperature", filename, "r", &comm);
    adios_groupsize = 0;
    adios_totalsize = 0;
    adios_group_size (adios_handle, adios_groupsize, &adios_totalsize);
    adios_buf_size = 4;

    // TODO actually adios_read() will read everything but the values
    // will be available after adios_close() for use; so here
    // I cheat, as it is cheated in examples/C/manual/3_adios_read.c
    // and variables NX, X, Y, are upfront set to the correct values
    // I don't know if this is a bug or an intended behavior
    adios_read (adios_handle, "NX", &NX, adios_buf_size);
    adios_read (adios_handle, "NX", &nx, adios_buf_size);
    printf("rank=%d, nx=%d\n", rank, nx);

    adios_buf_size = 8 * (NX);
    adios_read (adios_handle, "temperature", t, adios_buf_size);

    // for readint the size of the 2d variable
    adios_buf_size = sizeof(int);
    adios_read (adios_handle, "X", &X, adios_buf_size);
    // the size for Y size is the same as for X
    adios_read (adios_handle, "Y", &Y, adios_buf_size);
    // this size we will need to read the variable
    // and this we need to reserve
    adios_buf_size = sizeof(double) * X * Y;
    printf("rank=%d, X=%d, Y=%d, adios_buf_size=%d\n", rank, X, Y, adios_buf_size);
    void * p_buf_size = malloc(adios_buf_size);
    adios_read (adios_handle, "t_2Dim", p_buf_size, adios_buf_size);

    adios_buf_size = sizeof(double) * X * Y * Y;
    void * p_buf_size_3d = malloc(adios_buf_size);
    adios_read (adios_handle, "t_3Dim", p_buf_size_3d, adios_buf_size);

    adios_close (adios_handle);

    // TODO adios_close() cause flushing some internal buffering
    // and now all variables are visible and ready to use
    adios_finalize (rank);
    MPI_Finalize ();

    // print 1D
    sprintf(result, "1D: rank=%d t=[%g", rank, t[0]);
    for (i=1; i<NX; i++) {
        sprintf (s, ",%g", t[i]);
        strcat (result, s);
    }
    printf("%s]\n", result);

    // print 2D
    sprintf(result, "2D: rank=%d X=%d Y=%d, t_2d=[", rank, X, Y);
    double *p_double_2d = (double*) p_buf_size;

    for (i = 0; i < X*Y; i++){
    	sprintf (s, ",%g", p_double_2d[i]);
    	strcat (result, s);
    }

    printf("%s]\n", result);

    // print 3D
    sprintf(result, "3D: rank=%d X=%d Y=%d Y=%d, t_3d=[", rank, X, Y, Y);
    double *p_double_3d = (double*) p_buf_size_3d;

    for (i = 0; i < X*Y*Y; i++){
    	sprintf (s, ", %g", p_double_3d[i]);
    	strcat (result, s);
    }

    printf("%s]\n", result);


    free(p_buf_size);
    p_buf_size = NULL;

    free(p_buf_size_3d);
    p_buf_size_3d = NULL;

    return 0;
}
