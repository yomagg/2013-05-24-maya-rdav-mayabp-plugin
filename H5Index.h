#include <string>

namespace H5Index
{
  using     std::string;

  string indexFilename(const string &file);
  bool haveIndex(const string &file);
}
